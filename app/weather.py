import requests
from datetime import datetime

class WeatherSession:
    def __init__(self, api_key):
        self.api_key = api_key
        self.session = "winter"

    def get_weather(self, city: str):
        """
        Fetches the current weather for a given city from OpenWeatherMap API.

        Args:
            city (str): The city to get the weather for.

        Returns:
            dict: The current weather information in the city.
        """
        response = requests.get(f"http://api.openweathermap.org/data/2.5/weather?q={city}&appid={self.api_key}")
        data = response.json()
        
        # Getting the current temperature in Celsius
        temperature = data["main"]["temp"] - 273.15
        
        # Getting the current humidity
        humidity = data["main"]["humidity"]
        
        # Getting the current weather condition
        condition = data["weather"][0]["main"]

        # Getting the current date and day of the week
        date = datetime.now().strftime("%Y-%m-%d")
        day = datetime.now().strftime("%A")

        return temperature, humidity, condition, date, day

    def recommend_clothing(self, temperature: float) -> str:
        """
        Recommends clothing based on temperature.

        Args:
            temperature (float): The current temperature.

        Returns:
            str: Clothing recommendation.
        """
        if temperature > 30:
            return "Shorts and a t-shirt"
        elif temperature > 20:
            return "Long pants and a t-shirt"
        elif temperature > 10:
            return "Long pants and a sweater"
        else:
            return "Winter coat and pants"

    def get_clothing_recommendations(self, condition, season):
        if 'rain' in condition.lower():
            return 'Remember to bring an umbrella and wear a raincoat.'
        elif 'snow' in condition.lower():
            return 'Bundle up in warm layers and wear a waterproof jacket.'
        elif 'sunny' in condition.lower():
            if season == 'summer':
                return 'Wear sunscreen, a hat, and sunglasses to protect from the sun.'
            else:
                return 'Wear light and breathable clothing.'
        elif 'clouds' in condition.lower():
            return 'Opt for light layers and carry a light jacket in case of cooler temperatures.'
        elif 'windy' in condition.lower():
            return 'Wear wind-resistant clothing and avoid loose-fitting garments.'
        elif 'fog' in condition.lower():
            return 'Wear bright or reflective clothing to improve visibility in foggy conditions.'
        elif 'thunderstorm' in condition.lower():
            return 'Stay indoors and avoid unnecessary travel. If going out, wear waterproof clothing.'
        else:
            return 'Choose comfortable clothing suitable for the weather.'

    def get_season(self):
        """
        Calculates the current season based on month. This is a simple approximation and 
        assumes the Northern Hemisphere. In reality, the start and end dates of the 
        seasons can vary slightly each year and are different in the Southern Hemisphere.

        Returns:
            str: The current season.
        """
        month = datetime.now().month
        if month in [12, 1, 2]:
            return "winter"
        elif month in [3, 4, 5]:
            return "spring"
        elif month in [6, 7, 8]:
            return "summer"
        else:
            return "fall"
