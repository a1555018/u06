import unittest
from weather import WeatherSession

class TestWeatherSession(unittest.TestCase):
    def setUp(self):
        self.ws = WeatherSession('02d5a83f0206421e3e7f5d0d26bf3351')

    def test_get_weather(self):
        # call the method
        temperature, humidity, condition, date, day = self.ws.get_weather('London')

        # test the results
        self.assertIsInstance(temperature, float)
        self.assertIsInstance(humidity, (int, float))
        self.assertIsInstance(condition, str)
        self.assertIsInstance(date, str)
        self.assertIsInstance(day, str)

    def test_get_season(self):
        season = self.ws.get_season()
        self.assertIn(season.lower(), [s.lower() for s in ['Spring', 'Summer', 'Autumn', 'Winter']])

    # Add more tests for each method in your WeatherSession class

if __name__ == '__main__':
    unittest.main()
