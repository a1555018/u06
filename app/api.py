from flask import Flask, jsonify
from weather import WeatherSession

app = Flask(__name__)
ws = WeatherSession('02d5a83f0206421e3e7f5d0d26bf3351')  # Replace with your OpenWeatherMap API key

@app.route('/recommendation/<city>', methods=['GET'])
def get_recommendation(city: str):
    season = ws.get_season()
    temperature, humidity, condition, date, day = ws.get_weather(city)
    recommendation = ws.get_clothing_recommendations(condition, season)
    return jsonify({
        "day" : day,
        "date" : date,
        'city': city, 
        'recommendation': recommendation, 
        "temperature":temperature,
        "condition":condition,
        "humidity" : humidity,
        "season" : season
        })

if __name__ == "__main__":
    app.run(debug=True)
