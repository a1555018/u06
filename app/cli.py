import argparse
from weather import WeatherSession

def main():
    parser = argparse.ArgumentParser(description='Get clothing recommendations for a city.')
    parser.add_argument('city', type=str, help='The city to get recommendations for')

    

    args = parser.parse_args()

    api_key = "02d5a83f0206421e3e7f5d0d26bf3351"  # Replace with your OpenWeatherMap API key

    ws = WeatherSession(api_key)  # Replace with your OpenWeatherMap API key

    season = ws.get_season()
    temperature, humidity, condition, date, day = ws.get_weather(args.city)
    recommendation = ws.get_clothing_recommendations(condition, season)  

    print(f"In {args.city},Temperature is {temperature} it is recommended to wear: {recommendation}")

if __name__ == "__main__":
    main()
